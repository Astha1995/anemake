//
//  UsageGuidlineCell.swift
//  Animake
//
//  Created by Komal Gupta on 16/05/20.
//  Copyright © 2020 Ankur Purwar. All rights reserved.
//

import UIKit

class UsageGuidlineCell: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
