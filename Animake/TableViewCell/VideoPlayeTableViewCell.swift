//
//  VideoPlayeTableViewCell.swift
//  Animake
//
//  Created by Komal Gupta on 19/05/20.
//  Copyright © 2020 Ankur Purwar. All rights reserved.
//

import UIKit

class VideoPlayeTableViewCell: UITableViewCell {

    @IBOutlet weak var videoCollectionView: UICollectionView!
     var tableIndex = Int()
    override func awakeFromNib() {
        super.awakeFromNib()
       
        videoCollectionView.delegate = self
        videoCollectionView.dataSource = self
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//MARK:- collectionview delegate datasource
extension VideoPlayeTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource,  UICollectionViewDelegateFlowLayout
{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch tableIndex {
        case 0:
            return 6
        case 1:
            return 4
            
        default:
            return 2
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
       let cell = videoCollectionView.dequeueReusableCell(withReuseIdentifier: "VideoPlayeCollectionViewCell", for: indexPath) as? VideoPlayeCollectionViewCell
        
       //    cell?.rechargeImage.image = UIImage(named: arrProductImage[indexPath.item])
     //           cell?.nameLbl.text = arrRechargeName[indexPath.row]
             return cell!
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch tableIndex {
        case 0, 1:
            let width = (self.videoCollectionView.frame.width - 30) / 5
            let height = (self.videoCollectionView.frame.height - 10) / 1
            return CGSize(width: width, height: height)
      
        default:
            let width = (self.videoCollectionView.frame.width - 30) / 6
            let height = (self.videoCollectionView.frame.height - 10) / 1
            return CGSize(width: width, height: height)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
         switch tableIndex {
        case 0, 1:
            return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)

          default:
            return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)

          }
        
    }
    

}

