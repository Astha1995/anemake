//
//  ReusableTableViewCell.swift
//  Animake
//
//  Created by Ankur Purwar on 18/05/20.
//  Copyright © 2020 Ankur Purwar. All rights reserved.
//

import UIKit

class ReusableTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
class countryCell: UITableViewCell{
    
    @IBOutlet weak var imgCountryLogo: UIImageView!
    
    @IBOutlet weak var lblName: UILabel!
    
}
