
import UIKit

class SlideSampleCell: UICollectionViewCell {
    
}

class FirstCell: UICollectionViewCell {
    @IBOutlet weak var sampleCollectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.sampleCollectionView.register(UINib(nibName: "commonVideoNameCell", bundle: nil), forCellWithReuseIdentifier: "commonVideoNameCell")
        sampleCollectionView.delegate = self
        sampleCollectionView.dataSource = self
    }
    
}

//MARK:- collectionview delegate datasource
extension FirstCell: UICollectionViewDelegate, UICollectionViewDataSource,  UICollectionViewDelegateFlowLayout
{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
             return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
       let cell = sampleCollectionView.dequeueReusableCell(withReuseIdentifier: "commonVideoNameCell", for: indexPath) as? commonVideoNameCell
       //    cell?.rechargeImage.image = UIImage(named: arrProductImage[indexPath.item])
     //           cell?.nameLbl.text = arrRechargeName[indexPath.row]
             return cell!
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (self.sampleCollectionView.frame.width - 10) / 2
        let height = (self.sampleCollectionView.frame.height - 10) / 3
        return CGSize(width: width, height: height)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
    

}

class SecondCell: UICollectionViewCell {
    
    @IBOutlet weak var usaseGuideTblView: UITableView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        usaseGuideTblView.register(UINib(nibName: "UsageGuidlineCell", bundle: nil), forCellReuseIdentifier: "UsageGuidlineCell")
        usaseGuideTblView.delegate = self
        usaseGuideTblView.dataSource = self
    }
}

extension SecondCell : UITableViewDataSource,UITableViewDelegate{
        
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
       
            let cell = usaseGuideTblView.dequeueReusableCell(withIdentifier: "UsageGuidlineCell", for: indexPath) as! UsageGuidlineCell
            
            return cell

    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
            return 180
    }
    
//   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//      let obj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FoodDetailVC") as! FoodDetailVC
//            self.navigationController?.pushViewController(obj, animated: true)
//        }
//
}
