//
//  commonVideoNameCell.swift
//  Animake
//
//  Created by Ankur Purwar on 15/05/20.
//  Copyright © 2020 Ankur Purwar. All rights reserved.
//

import UIKit

class commonVideoNameCell: UICollectionViewCell {
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
