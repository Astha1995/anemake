//
//  ModalClass.swift
//  ylaa
//
//  Created by Younggeeks Technologies on 30/08/19.
//  Copyright © 2019 Younggeeks Technologies. All rights reserved.
//

import Foundation


// MARK: - Welcome
struct GetResponse_HomeListing: Codable {
    let result: Int?
    let latestProducts: [latestProducts]?
    let popularProducts: [PopularProduct]?
    let productListing: [ProductListing]?
    
    enum CodingKeys: String, CodingKey {
        case result
        case latestProducts = "latest_products"
        case popularProducts = "popular_products"
        case productListing = "product_listing"
    }
}

// MARK: - PopularProduct
struct PopularProduct: Codable {
    let productID, orderID, sellerID, rating: String?
    let imageDefault, productQuantity, productUnitPrice, productTotalPrice: String?
    let popularProductDescription, productTitle: String?
    
    enum CodingKeys: String, CodingKey {
        case productID = "product_id"
        case orderID = "order_id"
        case sellerID = "seller_id"
        case rating
        case imageDefault = "image_default"
        case productQuantity = "product_quantity"
        case productUnitPrice = "product_unit_price"
        case productTotalPrice = "product_total_price"
        case popularProductDescription = "description"
        case productTitle = "product_title"
    }
}

// MARK: - ProductListing
struct ProductListing: Codable {
    let rating, id, categoryID, title: String?
    let price, imageBig, userID, productListingDescription: String?
    let currency: String?
    let productType: ProductType?
    let createdAt: String?
    
    enum CodingKeys: String, CodingKey {
        case rating, id
        case categoryID = "category_id"
        case title, price
        case imageBig = "image_big"
        case userID = "user_id"
        case productListingDescription = "description"
        case currency
        case productType = "product_type"
        case createdAt = "created_at"
    }
}
enum ProductType: String, Codable {
    case physical = "physical"
}

struct latestProducts: Codable {
    let id, imageDefault, title, price: String?
    let welcomeDescription, rating, userID, userUsername: String?
    let shopName, userRole, userSlug, createdAt: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case imageDefault = "image_default"
        case title, price
        case welcomeDescription = "description"
        case rating
        case userID = "user_id"
        case userUsername = "user_username"
        case shopName = "shop_name"
        case userRole = "user_role"
        case userSlug = "user_slug"
        case createdAt = "created_at"
    }
}
struct GetResponse_LoginData: Codable {
    let result: Int?
    let data: DataClass_LoginData?
    let msg: String?
}





// MARK: - Welcome
class GetResponse_SellerPage: Codable {
    var result: Int?
    var msg: String?
    var data: [Datum_SellerPage]?

    init(result: Int?, msg: String?, data: [Datum_SellerPage]?) {
        self.result = result
        self.msg = msg
        self.data = data
    }
}

// MARK: - Datum
class Datum_SellerPage: Codable {
    var id, sellerStatus, quantity, datumDescription: String?
    var rating, hit, categoryID, createdAt: String?
    var title, price, image, shippingCost: String?
    var shippingTime, isSold: String?

    enum CodingKeys: String, CodingKey {
        case id
        case sellerStatus = "seller_status"
        case quantity
        case datumDescription = "description"
        case rating, hit
        case categoryID = "category_id"
        case createdAt = "created_at"
        case title, price, image
        case shippingCost = "shipping_cost"
        case shippingTime = "shipping_time"
        case isSold = "is_sold"
    }

    init(id: String?, sellerStatus: String?, quantity: String?, datumDescription: String?, rating: String?, hit: String?, categoryID: String?, createdAt: String?, title: String?, price: String?, image: String?, shippingCost: String?, shippingTime: String?, isSold: String?) {
        self.id = id
        self.sellerStatus = sellerStatus
        self.quantity = quantity
        self.datumDescription = datumDescription
        self.rating = rating
        self.hit = hit
        self.categoryID = categoryID
        self.createdAt = createdAt
        self.title = title
        self.price = price
        self.image = image
        self.shippingCost = shippingCost
        self.shippingTime = shippingTime
        self.isSold = isSold
    }
}






// MARK: - DataClass
struct DataClass_LoginData: Codable {
    let id, state, address2, phoneNumber: String?
    let zipCode, walletAmount, refralCode, password: String?
    let image, city, username, address1: String?
    let deviceID: Int?
    let email, name, country: String?
    
    enum CodingKeys: String, CodingKey {
        case id, state
        case address2 = "address_2"
        case phoneNumber = "phone_number"
        case zipCode = "zip_code"
        case walletAmount = "wallet_amount"
        case refralCode = "refral_code"
        case password, image, city, username
        case address1 = "address_1"
        case deviceID = "device_id"
        case email, name, country
    }
}
struct GetResponse_OrderListing: Codable {
    let result: Int?
    let data: [Data_order]?
    let msg: String?
    let status : String?
}
struct Data_order : Codable {
   let id : String?
   let order_number : String?
   let buyer_id : String?
   let price_subtotal : String?
   let buyer_type : String?
   let payment_method : String?
   let created_at : String?
   let price_shipping : String?
  let  price_total : String?
   let payment_status : String?
   let price_currency : String?
   let updated_at : String?
   let status : String?
}
// MARK: - Welcome
struct GetResponse_orderdetails: Codable {
    let billingCity, msg, billingAddress1, billingZipCode: String?
    let billingState, billingPhoneNumber, shippingEmail, paymentStatus: String?
    let orderStatus, billingCountry, shippingState, orderCreated: String?
    let shippingAddress2: String?
    let shippingLastName: String?
    let shippingCountry, shippingFirstName, billingFirstName, shippingCity: String?
    let paymentMethod, shippingAddress1, shippingZipCode, shippingPhoneNumber: String?
    let result: Int?
    let billingEmail: String?
    let data: [Datum_orderdetails]?
    let billingLastName: String?
    let billingAddress2: String?
    
    enum CodingKeys: String, CodingKey {
        case billingCity = "billing_city"
        case msg
        case billingAddress1 = "billing_address_1"
        case billingZipCode = "billing_zip_code"
        case billingState = "billing_state"
        case billingPhoneNumber = "billing_phone_number"
        case shippingEmail = "shipping_email"
        case paymentStatus = "payment_status"
        case orderStatus = "order_status"
        case billingCountry = "billing_country"
        case shippingState = "shipping_state"
        case orderCreated = "order_created"
        case shippingAddress2 = "shipping_address_2"
        case shippingLastName = "shipping_last_name"
        case shippingCountry = "shipping_country"
        case shippingFirstName = "shipping_first_name"
        case billingFirstName = "billing_first_name"
        case shippingCity = "shipping_city"
        case paymentMethod = "payment_method"
        case shippingAddress1 = "shipping_address_1"
        case shippingZipCode = "shipping_zip_code"
        case shippingPhoneNumber = "shipping_phone_number"
        case result
        case billingEmail = "billing_email"
        case data
        case billingLastName = "billing_last_name"
        case billingAddress2 = "billing_address_2"
    }
}

// MARK: - Datum
struct Datum_orderdetails: Codable {
    let productTitle, productType, storage, imageBig: String?
    let transactionID, productTotalPrice, buyerID, orderStatus: String?
    let updatedAt, buyerType, paymentAmount, imageSmall: String?
    let productSlug: String?
    let imageDefault, orderID, productShippingCost, id: String?
    let shippingTrackingNumber: String?
    let productID: String?
    let commissionRate: String?
    let isMain, productCurrency, productUnitPrice, createdAt: String?
    let shippingTrackingURL: String?
    let sellerID, productQuantity, senderID, isApproved: String?
    
    enum CodingKeys: String, CodingKey {
        case productTitle = "product_title"
        case productType = "product_type"
        case storage
        case imageBig = "image_big"
        case transactionID = "transaction_id"
        case productTotalPrice = "product_total_price"
        case buyerID = "buyer_id"
        case orderStatus = "order_status"
        case updatedAt = "updated_at"
        case buyerType = "buyer_type"
        case paymentAmount = "paymeGetResponse_LoginData"
        case imageSmall = "image_small"
        case productSlug = "product_slug"
        case imageDefault = "image_default"
        case orderID = "order_id"
        case productShippingCost = "product_shipping_cost"
        case id
        case shippingTrackingNumber = "shipping_tracking_number"
        case productID = "product_id"
        case commissionRate = "commission_rate"
        case isMain = "is_main"
        case productCurrency = "product_currency"
        case productUnitPrice = "product_unit_price"
        case createdAt = "created_at"
        case shippingTrackingURL = "shipping_tracking_url"
        case sellerID = "seller_id"
        case productQuantity = "product_quantity"
        case senderID = "sender_id"
        case isApproved = "is_approved"
    }
}
// MARK: - Welcome
struct GetResponse_TermsAndConditions: Codable {
    let result: Int?
    let termsListing: [TermsListing_TermsAndConditions]?
    
    enum CodingKeys: String, CodingKey {
        case result
        case termsListing = "terms_listing"
    }
}

// MARK: - TermsListing
struct TermsListing_TermsAndConditions: Codable {
    let id, terms, policy, updatedAt: String?
    let createdAt: String?
    
    enum CodingKeys: String, CodingKey {
        case id, terms, policy
        case updatedAt = "updated_at"
        case createdAt = "created_at"
    }
}



struct GetResponse_productListing: Codable {
    let result: Int?
    let msg: String?
    let data: [ProductListing]?
}
struct Data_productListing : Codable{
   let id : String?
  let  quantity : String?
   let description : String?
   let rating : String?
   let hit : String?
   let category_id : String?
   let created_at : String?
   let title : String?
   let price : String?
   let image : String?
   let shipping_cost : String?
   let shipping_time : String?
    let is_sold : String?
}
struct GetResponse_categoriesListing: Codable {
    let result: Int?
    let msg: String?
    let data: [Datum_categoriesListing]?
}

// MARK: - Datum
struct Datum_categoriesListing: Codable {
    let id, categoryName, image: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case categoryName = "Category Name"
        case image
    }
}
struct GetResponse_SubscriptionList: Codable {
    let result: Int?
    let data: [Datum_SubscriptionList]?
    let msg: String?
}

// MARK: - Datum
struct Datum_SubscriptionList: Codable {
    let id, plan, type, price: String?
    let duration, addProducts, status, createdAt: String?
    
    enum CodingKeys: String, CodingKey {
        case id, plan, type, price, duration
        case addProducts = "add_products"
        case status
        case createdAt = "created_at"
    }
}
struct GetResponse_CheckUserRole: Codable {
    let result: Int?
    let userRole, planID, planStatus, planAmount: String?
    let planName, planType, planDuration, planCreated: String?
    let planExpiry, addProducts, planNotification, msg: String?
    
    enum CodingKeys: String, CodingKey {
        case result
        case userRole = "user_role"
        case planID = "plan_id"
        case planStatus = "plan_status"
        case planAmount = "plan_amount"
        case planName = "plan_name"
        case planType = "plan_type"
        case planDuration = "plan_duration"
        case planCreated = "plan_created"
        case planExpiry = "plan_expiry"
        case addProducts = "add_products"
        case planNotification = "plan_notification"
        case msg
    }
}


struct GetResponse_ProductDetails: Codable {
    let result: Int?
    let data: [Datum_ProductDetails]?
    let productAttributes: [ProductAttribute_ProductDetails]?
    let images: [String]?
    let productReviews: [ProductReview_ProductDetails]?
    let productOrdered : String?
    let coutries : [Datum_countries]?
    enum CodingKeys: String, CodingKey {
        case result, data
        case productAttributes = "product_attributes"
        case images,coutries
        case productReviews = "product_reviews"
        case productOrdered = "product_ordered"
    }
}

struct Datum_countries : Codable {
    var city : String?
    var country : String?
    var state : String?
}


// MARK: - Datum
struct Datum_ProductDetails: Codable {
    var datumDescription, sellerID: String?
    var shippingTime: String?
    var shippingCostType, title, stateID, categoryName: String?
    var image, productCondition, cityID, createdAt: String?
    var currency, shippingCost, id, countryID: String?
    var zipCode, quantity, phoneNumber, hit: String?
    var price, ddkWalletID, rating, address: String?
    var username: String?
    var categoryId : String?
    
    enum CodingKeys: String, CodingKey {
        case datumDescription = "description"
        case sellerID = "seller_id"
        case shippingTime = "shipping_time"
        case shippingCostType = "shipping_cost_type"
        case title
        case stateID = "state_id"
        case categoryName = "category_name"
        case image
        case productCondition = "product_condition"
        case cityID = "city_id"
        case createdAt = "created_at"
        case currency
        case shippingCost = "shipping_cost"
        case id
        case countryID = "country_id"
        case zipCode = "zip_code"
        case quantity
        case phoneNumber = "phone_number"
        case hit, price
        case ddkWalletID = "ddk_wallet_id"
        case rating, address, username
        case categoryId = "category_id"
    }
}

// MARK: - ProductAttribute
struct ProductAttribute_ProductDetails: Codable {
    let attributeName, attributeVal: String?
    
    enum CodingKeys: String, CodingKey {
        case attributeName = "attribute_name"
        case attributeVal = "attribute_val"
    }
}

// MARK: - ProductReview
struct ProductReview_ProductDetails: Codable {
    let productID, userID, username, rating: String?
    let review, createdAt: String?
    
    enum CodingKeys: String, CodingKey {
        case productID = "product_id"
        case userID = "user_id"
        case username, rating, review
        case createdAt = "created_at"
    }
}

struct Getresponse_Country: Codable {
    let result: Int?
    let countries: [Country]?
    let states: [State]?
    let cities: [City]?
}

// MARK: - City
struct City: Codable {
    let  name, stateID: String?
     let id:Int?
    enum CodingKeys: String, CodingKey {
        case id, name
        case stateID = "state_id"
    }
}

// MARK: - Country
struct Country: Codable {
    let id, sortname, name, phonecode: String?
}

// MARK: - State
struct State: Codable {
    let name, countryID: String?
    let id:Int?
    enum CodingKeys: String, CodingKey {
        case id, name
        case countryID = "country_id"
    }
}
// MARK: - access Token

struct AccessToken: Codable {
    let accesstoken : String?
    let userId: Int
    enum CodingKeys : String, CodingKey {
        case accesstoken = "access_token"
        case userId
    }
}

struct Category: Codable {
    var id: Int?
    var name : String?
    var description : String?
   var active : Int?
    
    enum CodingKeys : String, CodingKey {
        case name = "name"
        case description = "description"
        case active = "active"
        case id = "id"
        
    }
}





/*
// dashboard
struct dashBoard : Codable {

    let fname,lname,email,mobile,city : String?
    let gender,dob,zodiacsign,jobtitle,modality,about : String?
    let profilepic,roleid,registeras_business,totalamount,wellness : String?
    let welljourney,wellsession,createdat : String?
    let updatedat,status,savefor_later,emailstatus : String?
    let role: [Role]?
    let city_id,role_changed : Int?
    
    enum CodingKeys: String, CodingKey {
        
    //    case id = "description"
        case fname = "f_name"
        case lname = "l_name"
        case emailstatus = "email_status"
        case email = "email"
        case mobile = "mobile"
        case city = "city"
  
        case gender = "gender"
        case dob = "dob"
        case zodiacsign = "zodiac_sign"
        case jobtitle = "job_title"
        case modality = "modality"
   //     case id
        case profilepic = "profile_pic"
        case about = "about"
        case roleid = "role_id"
        case registeras_business = "register_as_business"
        case totalamount = "total_amount"
   //    case hit, price
        case wellness = "wellness"
  //      case rating, address, username
        case welljourney = "well_journey"
        case wellsession = "well_session"
        case createdat = "created_at"
         case updatedat = "updated_at"
         case status
         case savefor_later
        
    }
    
}
    struct Role: Codable {
        let name, description: String?
        let id:Int?
        enum CodingKeys: String, CodingKey {
            case id = "id"
            case name = "name"
            case description = "description"
        }
    }
 */
  /*
     "f_name": "testing7",
     "l_name": "test",
     "email": "testing7@gmail.com",
     "email_status": 1,
     "mobile": "343435345345",
     "city": null,
     "city_id": 7,
     "state": null,
     "state_id": 5,
     "gender": "Female",
     "dob": null,
     "zodiac_sign": null,
     "job_title": null,
     "modality": null,
     "about": null,
     "profile_pic": null,
     "role_id": "2",
     "register_as_business": 1,
     "role_changed": 0,
     "total_amount": "0",
     "journey_start": "\"Just Curios\"",
     "wellness": "Done",
     "well_journey": "Fine",
     "well_session": "Done",
     "created_at": "2020-04-18 10:40:05",
     "updated_at": "2020-04-18 11:01:05",
     "status": 1,
     "save_for_later": 0,
     "role": {
         "id": 2,
         "name": "Business",
         "description": "Business Role"
     }
 
 
}
*/

struct GetResponse_CategoryAttributes: Codable {
    let result: Int?
    let productAttributes: [ProductAttribute_CategoryAttributes]?
    
    enum CodingKeys: String, CodingKey {
        case result
        case productAttributes = "product_attributes"
    }
}

// MARK: - ProductAttribute
struct ProductAttribute_CategoryAttributes: Codable {
    let status, id, catID, attributes: String?
    let createdAt: String?
    
    enum CodingKeys: String, CodingKey {
        case status, id
        case catID = "cat_id"
        case attributes
        case createdAt = "created_at"
    }
}


struct GetResponse_Otp: Codable {
    let msg: String?
    let result: Int?
    let userID: String?
    let data: DataClass?
    enum CodingKeys: String, CodingKey {
        case msg, result,data
        case userID = "user_id"
    }
}
struct DataClass: Codable {
    let id, state, address2, phoneNumber: String?
    let zipCode, walletAmount, refralCode, password: String?
    let image, city, username, address1: String?
    let role: String?
    let deviceID: String?
    let email, name, country: String?
    let cityId,stateId,countryId : String?
    
    enum CodingKeys: String, CodingKey {
        case id, state
        case address2 = "address_2"
        case phoneNumber = "phone_number"
        case zipCode = "zip_code"
        case walletAmount = "wallet_amount"
        case refralCode = "refral_code"
        case password, image, city, username
        case address1 = "address_1"
        case role
        case deviceID = "device_id"
        case email, name, country
        case cityId = "city_id"
        case stateId = "state_id"
        case countryId = "country_id"
    }
}
struct GetResponse_MessageChatView: Codable {
    let result: Int?
    let chatList: [ChatList_MessageChatView]?
    
    enum CodingKeys: String, CodingKey {
        case result
        case chatList = "chat_list"
    }
}

// MARK: - ChatList
struct ChatList_MessageChatView: Codable {
    let senderID, recieverID, recieverName, senderName: String?
    let senderImg, receiverImg, productID, message: String?
    let createdAt, chatDay: String?
    
    enum CodingKeys: String, CodingKey {
        case senderID = "sender_id"
        case recieverID = "reciever_id"
        case recieverName = "reciever_name"
        case senderName = "sender_name"
        case senderImg = "sender_img"
        case receiverImg = "receiver_img"
        case productID = "product_id"
        case message
        case createdAt = "created_at"
        case chatDay = "chat_day"
    }
}
struct Getresponse_chatDetails: Codable {
    let result: Int?
    let chatDetails: [ChatDetail_chatDetails]?
    
    enum CodingKeys: String, CodingKey {
        case result
        case chatDetails = "chat_details"
    }
}

// MARK: - ChatDetail
struct ChatDetail_chatDetails: Codable {
    let id, senderID, recieverID, message: String?
    let productID, chatID, s1, s2: String?
    let createdAt: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case senderID = "sender_id"
        case recieverID = "reciever_id"
        case message
        case productID = "product_id"
        case chatID = "chat_id"
        case s1, s2
        case createdAt = "created_at"
    }
}
struct Getresponse_orderdetails: Codable {
    let result: Int?
    let ordersDetails: OrdersDetails_orderdetails?
    let orderID: Int?
    let shippingDetails: ShippingDetails_orderdetails?
    
    enum CodingKeys: String, CodingKey {
        case result
        case ordersDetails = "orders_details"
        case orderID = "order_id"
        case shippingDetails = "shipping_details"
    }
}

// MARK: - OrdersDetails
struct OrdersDetails_orderdetails: Codable {
    let status: Int?
    let paymentStatus, buyerID, buyerType, priceTotal: String?
    let priceShipping: String?
    let priceCurrency: String?
    let priceSubtotal: String?
    let orderNumber: Int?
    let paymentMethod: String?
    
    enum CodingKeys: String, CodingKey {
        case status
        case paymentStatus = "payment_status"
        case buyerID = "buyer_id"
        case buyerType = "buyer_type"
        case priceTotal = "price_total"
        case priceShipping = "price_shipping"
        case priceCurrency = "price_currency"
        case priceSubtotal = "price_subtotal"
        case orderNumber = "order_number"
        case paymentMethod = "payment_method"
    }
}

// MARK: - ShippingDetails
struct ShippingDetails_orderdetails: Codable {
    let billingCity, billingAddress1, shippingEmail, billingState: String?
    let billingPhoneNumber, billingZipCode, shippingState, billingCountry: String?
    let shippingAddress2: String?
    let shippingLastName: String?
    let orderID: Int?
    let shippingFirstName, shippingCountry, shippingCity, billingFirstName: String?
    let shippingAddress1, shippingZipCode, shippingPhoneNumber, billingEmail: String?
    let billingLastName: String?
    let billingAddress2: String?
    
    enum CodingKeys: String, CodingKey {
        case billingCity = "billing_city"
        case billingAddress1 = "billing_address_1"
        case shippingEmail = "shipping_email"
        case billingState = "billing_state"
        case billingPhoneNumber = "billing_phone_number"
        case billingZipCode = "billing_zip_code"
        case shippingState = "shipping_state"
        case billingCountry = "billing_country"
        case shippingAddress2 = "shipping_address_2"
        case shippingLastName = "shipping_last_name"
        case orderID = "order_id"
        case shippingFirstName = "shipping_first_name"
        case shippingCountry = "shipping_country"
        case shippingCity = "shipping_city"
        case billingFirstName = "billing_first_name"
        case shippingAddress1 = "shipping_address_1"
        case shippingZipCode = "shipping_zip_code"
        case shippingPhoneNumber = "shipping_phone_number"
        case billingEmail = "billing_email"
        case billingLastName = "billing_last_name"
        case billingAddress2 = "billing_address_2"
    }
}


struct Getresponse_EarningHistory: Codable {
    let result: Int?
   // let totalCommission: String
    let data: [Datum_EarningHistory]?
    
    enum CodingKeys: String, CodingKey {
        case result
       // case totalCommission = "total_commission"
        case data
    }
}

// MARK: - Datum
struct Datum_EarningHistory: Codable {
    let recieverID, reffralAmount, id, createdAt: String?
    let userID, planAmount, commisionAmount: String?
    
    enum CodingKeys: String, CodingKey {
        case recieverID = "reciever_id"
        case reffralAmount = "reffral_amount"
        case id
        case createdAt = "created_at"
        case userID = "user_id"
        case planAmount = "plan_amount"
        case commisionAmount = "commision_amount"
    }
}
// MARK: - Welcome
struct GetResponse_AddProduct: Codable { 
    var result: Int?
    var data: DataClass_AddProduct?
    var attributValues: [AttributValue]?
    var productID: Int?
    var images: Images?
    var msg: String?
    
    enum CodingKeys: String, CodingKey {
        case result, data
        case attributValues = "attribut_values"
        case productID = "product_id"
        case images, msg
    }
}

// MARK: - AttributValue
struct AttributValue: Codable {
    var productID: Int?
    var attributeVal, attributeName: String?
    
    enum CodingKeys: String, CodingKey {
        case productID = "product_id"
        case attributeVal = "attribute_val"
        case attributeName = "attribute_name"
    }
}

// MARK: - DataClass
struct DataClass_AddProduct: Codable {
    var title, categoryID, dataDescription, slug: String?
    var price, currency, productCondition, userID: String?
    var countryID, stateID, cityID, address: String?
    var zipCode, quantity: String?
    var shippingTime: String?
    var shippingCostType: String?
    var isDraft: Int?
    
    enum CodingKeys: String, CodingKey {
        case title
        case categoryID = "category_id"
        case dataDescription = "description"
        case slug, price, currency
        case productCondition = "product_condition"
        case userID = "user_id"
        case countryID = "country_id"
        case stateID = "state_id"
        case cityID = "city_id"
        case address
        case zipCode = "zip_code"
        case quantity
        case shippingTime = "shipping_time"
        case shippingCostType = "shipping_cost_type"
        case isDraft = "is_draft"
    }
}

// MARK: - Images
struct Images: Codable {
    var result: String?
    var data: Bool?
}

struct GetResponse_NotificationList: Codable {
    var result: Int?
    var logsListing: [LogsListing_NotificationList]?
    
    enum CodingKeys: String, CodingKey {
        case result
        case logsListing = "logs_listing"
    }
}

// MARK: - LogsListing
struct LogsListing_NotificationList: Codable {
    var activity, id, cretaedAt, userID: String?
    
    enum CodingKeys: String, CodingKey {
        case activity, id
        case cretaedAt = "cretaed_at"
        case userID = "user_id"
    }
}
struct GetResponse_wowhelps: Codable {
    let result: Int?
    let faqsListing: [FaqsListing_wowhelps]?

    enum CodingKeys: String, CodingKey {
        case result
        case faqsListing = "faqs_listing"
    }
}

// MARK: - FaqsListing
struct FaqsListing_wowhelps: Codable {
    let id, questions, answers, status: String?
    let updateAt, createdAt: String?

    enum CodingKeys: String, CodingKey {
        case id, questions, answers, status
        case updateAt = "update_at"
        case createdAt = "created_at"
    }
}
struct GetResponse_EditProfile: Codable {
    let result: Int?
    let data: DataClass_EditProfile?
}

// MARK: - DataClass
struct DataClass_EditProfile: Codable {
    let id, username, name, image: String?
    let email, address1, address2: String?
    let countryID, stateID, cityID: String?
    let country, state, city, zipCode: String?
    let phoneNumber: String?

    enum CodingKeys: String, CodingKey {
        case id, username, name, image, email
        case address1 = "address_1"
        case address2 = "address_2"
        case countryID = "country_id"
        case stateID = "state_id"
        case cityID = "city_id"
        case country, state, city
        case zipCode = "zip_code"
        case phoneNumber = "phone_number"
    }
}
