//
//  VideoPlayerVC.swift
//  Animake
//
//  Created by Komal Gupta on 19/05/20.
//  Copyright © 2020 Ankur Purwar. All rights reserved.
//

import UIKit

class VideoPlayerVC: UIViewController {

    @IBOutlet weak var videoTblView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        videoTblView.delegate = self
        videoTblView.dataSource = self
        // Do any additional setup after loading the view.
    }
    

}

extension VideoPlayerVC : UITableViewDataSource,UITableViewDelegate{
        
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
            let cell = videoTblView.dequeueReusableCell(withIdentifier: "VideoPlayeTableViewCell", for: indexPath) as! VideoPlayeTableViewCell
         cell.tableIndex = indexPath.row
            return cell

    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.row {
        case 0,1:
            return 80
        default:
            return 60
        }
            
    }
    
    
}
