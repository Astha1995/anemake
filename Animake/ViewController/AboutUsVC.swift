//
//  AboutUsVC.swift
//  Animake
//
//  Created by Komal Gupta on 17/05/20.
//  Copyright © 2020 Ankur Purwar. All rights reserved.
//

import UIKit

class AboutUsVC: UIViewController {

    @IBOutlet weak var lblAbout: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

     @IBAction func btnBackAction(_ sender: Any) {
      self.navigationController?.popViewController(animated: true)
     }
}
