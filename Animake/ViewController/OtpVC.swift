
import UIKit
import IBAnimatable

class OtpVC: UIViewController {

    @IBOutlet weak var txtFieldFirst: AnimatableTextField!
    @IBOutlet weak var txtFieldSecond: AnimatableTextField!
    @IBOutlet weak var txtFieldThird: AnimatableTextField!
    @IBOutlet weak var txtFieldFourth: AnimatableTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
    navigationController?.navigationBar.isHidden = true
        // Do any additional setup after loading the view.
    }
    

    @IBAction func verifyBtnAction(_ sender: Any) {
        let obj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "WelcomeAnimakeVC") as! WelcomeAnimakeVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
    

}
