//
//  ProfileVC.swift
//  Animake
//
//  Created by Komal Gupta on 17/05/20.
//  Copyright © 2020 Ankur Purwar. All rights reserved.
//

import UIKit
import IBAnimatable

class ProfileVC: UIViewController {

    @IBOutlet weak var lblContoryCode: UILabel!
    @IBOutlet weak var txtFieldUsername: AnimatableTextField!
    @IBOutlet weak var txtFieldNumber: AnimatableTextField!
    @IBOutlet weak var txtFieldEmail: AnimatableTextField!
    @IBOutlet weak var txtFieldGender: AnimatableTextField!
    @IBOutlet weak var imgProfile: AnimatableImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func btnBackAction(_ sender: Any) {
     self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSaveAction(_ sender: Any) {
    
    }

    @IBAction func btnCancleAction(_ sender: Any) {
    
    }
    
    @IBAction func btnGenerAction(_ sender: Any) {
       
       }

}
