
import UIKit

class WelcomeAnimakeVC: UIViewController {

    
    @IBOutlet weak var menuTableView: UITableView!
    @IBOutlet weak var viewMenu: UIView!
    @IBOutlet weak var btnMenu: UIButton!
    
    let  arrImage = ["about-us","Star","share_ic","help & sup-1","help & sup-2","help & sup-3"]
       
     let menuItems = ["About","Rate this App","Share this App","Buy Package","Profile","Logout"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewMenu.isHidden = true
        menuTableView.register(UINib(nibName : "DrawerCell",bundle : nil), forCellReuseIdentifier: "DrawerCell")
        menuTableView.delegate = self
        menuTableView.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnMenuAction(_ sender: Any) {
        if btnMenu.isSelected == false {
          viewMenu.isHidden = false
            btnMenu.isSelected = true
    } else {
        viewMenu.isHidden = true
            btnMenu.isSelected = false

    }
        
    }
    
    @IBAction func btnNotificationAction(_ sender: Any) {
        
    }
    
    @IBAction func BtnThreeDots(_ sender: Any) {
        
    }
    
}

extension WelcomeAnimakeVC : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = menuTableView.dequeueReusableCell(withIdentifier: "DrawerCell", for: indexPath) as! DrawerCell
        cell.lblTitle.text = menuItems[indexPath.row]
        cell.imgProfile.image = UIImage(named: arrImage[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}

extension WelcomeAnimakeVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
   //     tableView.deselectRow(at: indexPath, animated: true)
        switch indexPath.row {
        case 0:
            
           let obj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "AboutUsVC") as! AboutUsVC
           self.navigationController?.pushViewController(obj, animated: true)
        case 1:
           print("vc")
            let obj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "RatingVC") as! RatingVC
            self.navigationController?.pushViewController(obj, animated: true)
           // buyermode
        case 2:
            print("2")
            viewMenu.isHidden = true
            let string1 = "itms-apps://itunes.apple.com/app/idXXXXXXX"
            let url = NSURL(string: string1)

            let shareItems:Array = [UIApplication.shared.openURL(url! as URL)]
            let activityViewController:UIActivityViewController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
            self.present(activityViewController, animated: true, completion: nil)
        case 3:
            print("2")
           let obj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "BuyPakageVC") as! BuyPakageVC
           self.navigationController?.pushViewController(obj, animated: true)
      
        case 4 :
            print("2")
            let obj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "ProfileVC") as! ProfileVC
            self.navigationController?.pushViewController(obj, animated: true)
                
    
        case 5:
            print("logout")
            let alertController = UIAlertController(title: ("Logout") , message: "Do you want to logout", preferredStyle: .alert)
            let Ok = UIAlertAction(title:("Ok") , style: .default) { (action) in
             
             //   self.menuActionDelegate?.openSegue("logout", sender: nil)
            }
            let cancel = UIAlertAction(title:("Cancel") , style: .cancel) { (action) in }
            alertController.addAction(Ok)
            alertController.addAction(cancel)
            self.present(alertController, animated: true) { }
        
        default:
            break
        }
    }
}
