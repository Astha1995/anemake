//
//  RatingVC.swift
//  Animake
//
//  Created by Komal Gupta on 17/05/20.
//  Copyright © 2020 Ankur Purwar. All rights reserved.
//

import UIKit
import IBAnimatable

class RatingVC: UIViewController {

    @IBOutlet weak var txtViewComment: AnimatableTextView!
    @IBOutlet weak var imgProfile: AnimatableImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
       }

    @IBAction func btnSubmitAction(_ sender: Any) {
        let obj = UIStoryboard(name: "Base.toryboard", bundle: nil).instantiateViewController(identifier: "VideoPlayerVC") as! VideoPlayerVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
}
