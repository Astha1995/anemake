
import UIKit
import IBAnimatable
class NumberVerifyVC: UIViewController,UISearchBarDelegate {
    
    @IBOutlet weak var mainContainerView: UIView!
    @IBOutlet weak var imgContry: UIImageView!
    @IBOutlet weak var lblCountryCode: UILabel!
    @IBOutlet weak var txtFieldNumber: UITextField!
    @IBOutlet weak var viewCountry: UIView!
    
    
    @IBOutlet weak var txtFldsearch: UISearchBar!
    @IBOutlet weak var tblView: UITableView!
    var arrCountryDetails = [countryChoose]()
    var arrSearch = [countryChoose]()
    var selectedIndex = String()
    var resultSearchController = UISearchController()
    var isSearchActive = false
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = true
        viewCountry.isHidden = true
        
        arrCountryDetails = [countryChoose(countryName: "Afganistan", countryCode: "+93", image: "afghanistan"),countryChoose(countryName: "China", countryCode: "+86", image: "china"),countryChoose(countryName: "India", countryCode: "+91", image: "india"),countryChoose(countryName: "United Arab Emrites", countryCode: "+971", image: "uae"),countryChoose(countryName: "United States", countryCode: "+1", image: "usa")]
        
        imgContry.image =  UIImage(named: arrCountryDetails[2].image!)
        lblCountryCode.text =  arrCountryDetails[2].countryCode
        tblView.delegate =  self
        tblView.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnCancelCountryViewAction(_ sender: UIButton) {
        hideShowView(objView: viewCountry, hidden: true)
        mainContainerView.alpha = 1
        
    }
    @IBAction func btnCountryCodeAction(_ sender: Any) {
        
        hideShowView(objView: viewCountry, hidden: false)
        mainContainerView.alpha = 0.5
    }
    
    
    @IBAction func btnSubmitAction(_ sender: Any) {
        let obj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "OtpVC") as! OtpVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar){
        searchBar.showsCancelButton = false
        searchBar.text = ""
        //tblBottomConstraint.constant = 5
        self.view.endEditing(true)
        self.navigationController?.isNavigationBarHidden = false
        isSearchActive = false
        tblView.reloadData()
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool{
        
        let _: NSDictionary = [NSAttributedString.Key.foregroundColor: UIColor.black,NSAttributedString.Key.backgroundColor: UIColor.black]
        
        searchBar.showsCancelButton = false
        return true
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool{
        return true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        arrSearch.removeAll()
        
        let searchPredicate = NSPredicate(format: "SELF CONTAINS[c] %@", txtFldsearch.text!)
        let array = (arrCountryDetails.map{$0.countryName} as NSArray).filtered(using: searchPredicate)
        
        for i in array {
            
            let value = arrCountryDetails.filter{$0.countryName == i as? String}
            arrSearch.append(contentsOf: value)
        }
        
        
        // arrSearch = array as! [countryChoose]
        
        isSearchActive = searchText.count == 0 ? false : true
        tblView.reloadData()
    }
    
    
}


extension NumberVerifyVC: UITableViewDelegate,UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if (isSearchActive)
        {
            return arrSearch.count
        } else {
            return arrCountryDetails.count
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "countryCell") as! countryCell
        cell.selectionStyle = .none
        
        if isSearchActive {
            if selectedIndex == arrSearch[indexPath.row].countryName {
                cell.accessoryType = .checkmark
            }else{
                cell.accessoryType = .none
            }
            cell.lblName?.text = arrSearch[indexPath.row].countryName
            cell.imgCountryLogo.image = UIImage(named: arrSearch[indexPath.row].image!)
            
        }else{
            if selectedIndex == arrCountryDetails[indexPath.row].countryName {
                cell.accessoryType = .checkmark
            }else{
                cell.accessoryType = .none
            }
            cell.imgCountryLogo.image = UIImage(named: arrCountryDetails[indexPath.row].image!)
            cell.lblName.text = arrCountryDetails[indexPath.row].countryName
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isSearchActive {
            imgContry.image =  UIImage(named: arrSearch[indexPath.row].image!)
            lblCountryCode.text =  arrSearch[indexPath.row].countryCode
            
            
        }else{
            imgContry.image =  UIImage(named: arrCountryDetails[indexPath.row].image!)
            lblCountryCode.text =  arrCountryDetails[indexPath.row].countryCode
            
        }
        hideShowView(objView: viewCountry, hidden: true)
        mainContainerView.alpha = 1
        tblView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }

}

struct countryChoose {
    var countryName : String?
    var countryCode : String?
    var image : String?
    
}


