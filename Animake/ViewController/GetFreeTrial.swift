//
//  GetFreeTrial.swift
//  Animake
//
//  Created by Komal Gupta on 18/05/20.
//  Copyright © 2020 Ankur Purwar. All rights reserved.
//

import UIKit

class GetFreeTrial: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    let arrCategoryImage = ["Group 521", "Image 960"]
    let arrCategoryName = ["Convert New Video", "Continue With Draft"]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.register(UINib(nibName: "commonVideoNameCell", bundle: nil), forCellWithReuseIdentifier: "commonVideoNameCell")
        collectionView.delegate = self
        collectionView.dataSource = self
        // Do any additional setup after loading the view.
    }
    

}

//MARK:- collectionview delegate datasource
extension GetFreeTrial: UICollectionViewDelegate, UICollectionViewDataSource,  UICollectionViewDelegateFlowLayout
{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
             return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
       let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "commonVideoNameCell", for: indexPath) as? commonVideoNameCell
            cell?.imgIcon.image = UIImage(named: arrCategoryImage[indexPath.item])
            cell?.lblName.text = arrCategoryName[indexPath.row]
            cell?.lblName.textColor = UIColor.white
             return cell!
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (self.collectionView.frame.width - 10) / 1
        let height = (self.collectionView.frame.height - 30) / 2
        return CGSize(width: width, height: height)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
    

}
